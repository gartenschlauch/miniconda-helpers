@echo off
call "%~dp0miniconda-environment.bat"
set "OPTS=--yes"
@echo on
rem --- basics:
conda update %OPTS% conda
conda update %OPTS% --all
conda install %OPTS% menuinst
conda install %OPTS% mingw
conda install %OPTS% conda-server
conda install %OPTS% virtualenv
rem --- for maths and data processing:
conda install %OPTS% numpy pandas scipy matplotlib statsmodels
rem --- for excel reading:
conda install %OPTS% xlrd openpyxl
rem --- for spyder:
conda install %OPTS% wxpython
rem --- both for ipython-qtconsole:
conda install %OPTS% pyqt
conda install %OPTS% pygments pyzmq
conda install %OPTS% spyder ipython ipython-qtconsole
rem --- various useful tools:
conda install %OPTS% flake8
conda install %OPTS% dill
rem --- for memory-profiler:
conda install %OPTS% psutil
set "PIP_OPTS=--upgrade"
pip install %PIP_OPTS% winpdb
pip install %PIP_OPTS% autopep8
pip install %PIP_OPTS% memory-profiler
rem --- update all, credits to Ramana and SalmanPK from http://stackoverflow.com/questions/2720014/upgrading-all-packages-with-pip
rem python -c "import pip, subprocess; [subprocess.call('pip install %PIP_OPTS% {}'.format(d.project_name), shell=True) for d in pip.get_installed_distributions()]"
@echo off
