@echo off

rem --- setting install architecture x86_64 (64-bit) or x86 (32-bit)
rem this is only relevant for the setup program download choice

rem 64-bit:
set ARCH=x86_64
rem 32-bit:
rem set ARCH=x86

if "%ARCH%"=="x86_64" (
    set ARCH_STR=64-bit
) else if "%ARCH%"=="x86" (
    set ARCH_STR=32-bit
) else (
    set ARCH_STR=
)

echo installing Miniconda (%ARCH_STR%)...

rem --- set arch-dependent install dir: ---
rem set MYMINICONDA=%USERPROFILE%\Miniconda-%ARCH%

rem --- set MINICONDA target dir ---
call "%~dp0miniconda-environment.bat"

rem --- skip install if already done ---
if exist "%MINICONDA%\python.exe" echo miniconda already installed, skipping install && goto skipinstall

rem --- create HOME dir ---
if not exist "%HOME%" mkdir "%HOME%"

rem --- set anaconda non-admin flag ---
if not exist "%MINICONDA%\.nonadmin" echo. > "%MINICONDA%\.nonadmin"

rem --- create download directory ---
set DOWNLOAD=%HOME%\Setup
if not exist "%DOWNLOAD%" mkdir "%DOWNLOAD%"
set ORIGINAL=%~dp0

rem --- extract wget ---
pushd "%DOWNLOAD%"
set WGET=%DOWNLOAD%\wget\bin\wget.exe
if not exist "%WGET%" "%ORIGINAL%\7za.exe" x "%ORIGINAL%wget.zip"
popd

rem --- download and install anaconda ---
set URL=http://repo.continuum.io/miniconda/Miniconda-latest-Windows-%ARCH%.exe
set SETUP_FILE=%DOWNLOAD%\miniconda-setup-%ARCH%.exe
"%WGET%" -c --no-check-certificate -O "%SETUP_FILE%" %URL%

echo running miniconda setup silently...
"%SETUP_FILE%" /S /InstallationType=JustMe /AddToPath=0 /RegisterPython=0 /D=%MINICONDA%

echo installing icon for shell...

set MENUDIR=%MINICONDA%\Menu
if not exist "%MENUDIR%" mkdir "%MENUDIR%"
call :heredoc shellicon>"%MINICONDA%\Menu\miniconda-shell.json" && goto skipdoc1
{
  "menu_items": [
    { 
      "name": "Miniconda Shell", 
      "script": "Home/miniconda-shell.bat", 
      "scriptargument": "",
      "workdir": "${PREFIX}/Home"
    }
  ], 
  "menu_name": "Anaconda (!ARCH_STR!)"
}

:skipdoc1

"%MINICONDA%\python.exe" "%MINICONDA%\Lib\_nsis.py" mkmenus
echo done.

:skipinstall

rem --- copy these batch scripts ---

for %%f in (miniconda*.bat) do call:copy_if_not_exist %%f,%HOME%\%%~nxf

rem --- end of batch ---
goto :eof

rem --- helper script ---
:copy_if_not_exist
set FROM=%1
set TO=%2
if not exist "%TO%" copy "%FROM%" "%TO%"
goto :eof


rem heredoc function - credits to rojo from stackoverflow, http://stackoverflow.com/posts/15032476
:heredoc
setlocal enabledelayedexpansion
set go=
for /f "delims=" %%A in ('findstr /n "^" "%~f0"') do (
    set "line=%%A" && set "line=!line:*:=!"
    if defined go (if #!line:~1!==#!go::=! (goto :EOF) else echo(!line!)
    if "!line:~0,13!"=="call :heredoc" (
        for /f "tokens=3 delims=>^ " %%i in ("!line!") do (
            if #%%i==#%1 (
                for /f "tokens=2 delims=&" %%I in ("!line!") do (
                    for /f "tokens=2" %%x in ("%%I") do set "go=%%x"
                )
            )
        )
    )
)
goto :eof
