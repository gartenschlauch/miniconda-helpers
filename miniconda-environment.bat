@echo off

rem --- default miniconda root ---
set MINICONDA=%USERPROFILE%\Miniconda

rem --- override possible ---
if exist "%~dp0miniconda-override.bat" call "%~dp0miniconda-override.bat"
if defined MYMINICONDA set MINICONDA=%MYMINICONDA%

rem --- set path ---
set ADDPATH=%~dp0miniconda-addpath.bat
if exist "%ADDPATH%" goto addpath

:fallback
rem echo adding path via fallback
set PATH=%MINICONDA%;%MINICONDA%\Scripts;%PATH%
goto endif1

:addpath
rem echo adding path via helper script
set MINICONDA_SCRIPTS=%MINICONDA%\Scripts
call "%ADDPATH%" MINICONDA_SCRIPTS /B
call "%ADDPATH%" MINICONDA /B
goto endif1

:endif1

rem --- set and create home ---
set HOME=%MINICONDA%\Home
