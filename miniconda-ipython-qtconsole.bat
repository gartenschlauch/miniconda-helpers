@echo off
call "%~dp0miniconda-environment.bat"

set IP_CONF_DIR=%HOME%\.ipython\profile_default
set IP_CONF_FILE=%IP_CONF_DIR%\ipython_config.py

if exist "%IP_CONF_FILE%" goto skip_create_config
if not exist "%IP_CONF_DIR%" mkdir "%IP_CONF_DIR%"

call :heredoc ipythonconfig>"%IP_CONF_FILE%" && goto skipdoc
# ipython_config.py

c = get_config()

cmds = """
%matplotlib inline
%config InlineBackend.figure_format = 'svg'
%load_ext autoreload
%autoreload 2
%load_ext memory_profiler
import pandas as pd
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
"""

lines = cmds.replace('\r', '').strip().split('\n')
c.InteractiveShellApp.exec_lines = lines


:skipdoc

:skip_create_config

rem --- start ipython ---
start pythonw.exe "%MINICONDA%\Scripts\ipython-script.py" qtconsole

goto :EOF

rem heredoc function - credits to rojo from stackoverflow, http://stackoverflow.com/posts/15032476
:heredoc
setlocal enabledelayedexpansion
set go=
for /f "delims=" %%A in ('findstr /n "^" "%~f0"') do (
    set "line=%%A" && set "line=!line:*:=!"
    if defined go (if #!line:~1!==#!go::=! (goto :EOF) else echo(!line!)
    if "!line:~0,13!"=="call :heredoc" (
        for /f "tokens=3 delims=>^ " %%i in ("!line!") do (
            if #%%i==#%1 (
                for /f "tokens=2 delims=&" %%I in ("!line!") do (
                    for /f "tokens=2" %%x in ("%%I") do set "go=%%x"
                )
            )
        )
    )
)
goto :EOF
