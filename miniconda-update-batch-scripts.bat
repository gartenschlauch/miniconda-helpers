@echo off

setlocal
rem -- create copy of this in %TEMP% and run to be ableto overwrite the original ---
if /i not "%~dp0"=="%temp%\" (
  copy /y "%~dpnx0" "%temp%\%~nx0" >nul
  call "%~dp0miniconda-environment.bat"
  "%temp%\%~nx0"
  if exist "%temp%\%~nx0" del "%temp%\%~nx0"
  goto :eof
)

rem call %~dp0miniconda-environment.bat

rem --- create download directory ---
set DOWNLOAD=%HOME%\Setup
if not exist "%DOWNLOAD%" mkdir "%DOWNLOAD%"

rem --- check for wget ---
set WGET=%DOWNLOAD%\wget\bin\wget.exe
if not exist "%WGET%" echo wget missing in "%WGET%" && goto :eof

set REPO=https://bitbucket.org/gartenschlauch/miniconda-helpers

rem --- download 7-zip cmdline archiver ---
set SEVENZIP_DIR=%DOWNLOAD%\7za
set SEVENZIP=%SEVENZIP_DIR%\7za.exe
if not exist "%SEVENZIP_DIR%" mkdir "%SEVENZIP_DIR%"
if not exist "%SEVENZIP%" "%WGET%" --no-check-certificate -O "%SEVENZIP%" %REPO%/raw/master/7za.exe

rem --- download repo ---
set REPO_ZIP=%DOWNLOAD%\miniconda-helpers-master.zip
"%WGET%" --no-check-certificate -O "%REPO_ZIP%" %REPO%/get/master.zip


rem --- extract batch scripts ---
pushd "%HOME%"
set REPOFILES=%DOWNLOAD%\repofiles
"%SEVENZIP%" e -y -o"%REPOFILES%" "%REPO_ZIP%"
copy /y "%REPOFILES%\*.bat"
popd
