Miniconda Helper Scripts
========================

Overview
--------

This is a set of helper Batch files that can install
Miniconda and a set of packages on a Windows machine
by a single click.  
Administrator privileges are not required.  
The default target directory is `%USERPROFILE%\Miniconda`.


File list
---------

* `miniconda-addpath.bat`  Helper Script to extend `%PATH%` 
                           only when necessary.
* `miniconda-environment.bat`  Sets `%MINICONDA%` and, based 
                               on this, `%PATH%` and `%HOME%`.
* `miniconda-shell.bat`  Calls `miniconda-environment.bat` 
                         in a newly spawned `cmd.exe`.
* `miniconda-install.bat`  Downloads and installs Miniconda for 
                           desired architecture using bundled 
                           `wget`. Depends on `miniconda-environment.bat` 
                           for installation directory.  
                           Comment/uncomment the `set ARCH=...` lines
                           to choose 32bit/64bit binaries.
* `miniconda-packages.bat`  Installs a set of packages that I need.
                            Customize this to your needs.
* `miniconda-deploy-all.bat`  Calls Miniconda installer and 
                              the package install script.  
                              This is the one-click-to-set-up
                              script.
* `miniconda-spyder.bat`  Spyder launcher


Set installation directory
--------------------------

To set a different installation directory,
create a file named `miniconda-override.bat` in the same directory
as `miniconda-environment.bat` and
insert the line:

    set MYMINICONDA=C:\<point to desired install dir>
    
This will be included by `miniconda-environment.bat`


Things, that I have not checked for
-----------------------------------

* If installation directories containing spaces will work
  (I believe they won't)
* If all packages will succeed to install. I updated
  `miniconda-packages.bat` several times without testing
  each time.

