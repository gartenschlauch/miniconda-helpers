@echo off
rem --- run this from the helpers script collection to install miniconda + packages on any windows user profile ---
call "%~dp0miniconda-install.bat"
call "%~dp0miniconda-packages.bat"
echo all done
echo MINICONDA: %MINICONDA%
echo PATH: %PATH%
pause
